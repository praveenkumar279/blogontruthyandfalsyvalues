---
title: Truthy and Falsy values in JavaScript
date: 2023-02-24 12:46:04
tags:
---


When you are writing a code in javascript, you will come across these "truthy" and "falsy" terms. Truthy is referred as true in the context of boolean and falsy is referred to false in the context of boolean.

<h2> Truthy values </h2>

Truthy values is similar to the true in boolean i.e., true, 1, "hii" etc. We know that all the numbers, non-empty strings, non-empty objects will return the truthy values.

<h4> Examples : </h4>


- if(1)
* if(true)
+ if(-10)
* if(120)
+ if("hello")


```
   let number = 123;

   if(number){
      console.log("Number is a truthy value");
   }
   else{
      console.log("Number is a Falsy value");
   }


   // Output : Number is a truthy value
```


Actually, 0 and false are falsy values. But, "0" and "false" are truthy values.

<h4></b> Do you want to know that How those values are truthy.???? </b> </h4>

Here you go, if you give strings, it will calculate the length and return the length and it will check that if the length is greater than "Zero" (0) then it is considered as truthy values. But, in case of arrays and objects it will truthy value even if it is empty.
you can refer the below truthy values to get an idea.

- if("0")
+ if("false")
* if( { name:"john" } )
+ if( [ ] )
- if( { } )

<b> Code Example :</b>

```
   let value = "false";

   if(value){
      console.log("It is a truthy value");
   }
   else{
      console.log("It is a Falsy value");
   }


   // Output : It is a truthy value
```


```
   let objectDetails = ({ name : "Bob" , Age: 20});

   if(objectDetails){
      console.log("Given objectDetails is a truthy value");
   }
   else{
      console.log("Given objectDetails is a falsy value");
   }


   // Output : Given objectDetails is a truthy value


```


<h2> Falsy values </h2>

Falsy values is similar to the false in boolean i.e., 0 , false, null including all the empty strings also return falsy values.

<h4> Examples : </h4>

- if(0)
* if(false)
+ if(null)
* if(undefined)
+ if("hello")

As you know from truthy values concept, "0" and "false" are truthy values but "" is falsy value. <b> Both are strings right, Then why??? </b>
Yes, you are right!!! it will return the length of the string and if it's empty then it is a falsy value.

- <b> if("") </b> a falsy value. 

<b> Code Example :</b>

```
let emptyString = "";

if(emptyString){
   console.log("It is a truthy value);
}
else{
   console.log("It is a falsy value");
}


// Output : It is a falsy value, because emptyString returns false value
   
```



```
let value;

if(value){
   console.log("It is a truthy value");
}
else{
   console.log("It is a falsy value");
}


//output: It is a falsy value, becuase the value is undefined.
   
```

<h2> Conclusion </h2>

In conclusion, truthy and falsy values are fundamental concepts in javascript. I hope this blog has provided with you a clear understanding of truthy and falsy values.

